package com.kapta.batman.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.kapta.batman.data.entity.MovieData
import com.kapta.batman.data.entity.MovieListData
import com.kapta.batman.data.entity.SearchData

@Database(
    entities = [SearchData::class,MovieData::class], version = 1, exportSchema = true
)
abstract class AppDB : RoomDatabase() {
    abstract fun movieDao(): MovieDao
}
