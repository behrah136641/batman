package com.kapta.batman.data.db


import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.kapta.batman.data.entity.MovieData
import com.kapta.batman.data.entity.SearchData

@Dao
interface MovieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(movies: List<SearchData>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAMovie(movie: MovieData)

    @Query("SELECT * FROM search WHERE Title LIKE :s ORDER BY imdbID LIMIT :offset, 10")
    fun getMovieList(s: String,offset:Int): List<SearchData>?

    @Query("SELECT * FROM movie WHERE imdbID =:id")
    fun getMovieDetail(id: String): MovieData?

}