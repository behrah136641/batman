package com.kapta.batman.data.db.datasource.contacts

import com.kapta.batman.data.db.AppDB
import com.kapta.batman.data.entity.MovieData
import com.kapta.batman.data.entity.MovieListData
import com.kapta.batman.data.entity.SearchData
import javax.inject.Inject


class LocalMovieDataSource @Inject constructor(
    private val db: AppDB
) : MovieLDataSource {
    override suspend fun insertAll(movies: List<SearchData>) {
        return db.movieDao().insertAll(movies)
    }

    override suspend fun insertAMovie(movie: MovieData) {
        return db.movieDao().insertAMovie(movie)
    }

    override suspend fun getMovieList(s: String,page:String): MovieListData {
        val movieListData= MovieListData()
        val offset=(page.toInt()-1)*10
        val search=db.movieDao().getMovieList("%$s%",offset)
        movieListData.search=search
        movieListData.response="True"
        return movieListData
    }

    override suspend fun getMovieDetail(id: String): MovieData? {
        return db.movieDao().getMovieDetail(id)
    }

}