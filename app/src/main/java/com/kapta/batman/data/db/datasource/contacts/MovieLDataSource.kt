package com.kapta.batman.data.db.datasource.contacts

import com.kapta.batman.data.entity.MovieData
import com.kapta.batman.data.entity.MovieListData
import com.kapta.batman.data.entity.SearchData

interface MovieLDataSource {
    suspend fun insertAll(movies: List<SearchData>)
    suspend fun insertAMovie(movie:MovieData)
    suspend fun getMovieList(s: String,page:String): MovieListData?
    suspend fun getMovieDetail(id: String): MovieData?
}