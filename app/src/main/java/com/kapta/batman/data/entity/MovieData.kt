package com.kapta.batman.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity(tableName = "movie",primaryKeys = ["imdbID"])
data class MovieData(

    @SerializedName("imdbID")
    @ColumnInfo("imdbID")
    var imdbID: String = "0",

    @SerializedName("Title")
    @ColumnInfo("Title")
    var title: String? = null,

    @SerializedName("Year")
    @ColumnInfo("Year")
    var year: String? = null,

    @SerializedName("Rated")
    @ColumnInfo("Rated")
    var rated: String? = null,

    @SerializedName("Released")
    @ColumnInfo("Released")
    var released: String? = null,

    @SerializedName("Runtime")
    @ColumnInfo("Runtime")
    var runtime: String? = null,

    @SerializedName("Genre")
    @ColumnInfo("Genre")
    var genre: String? = null,

    @SerializedName("Director")
    @ColumnInfo("Director")
    var director: String? = null,

    @SerializedName("Writer")
    @ColumnInfo("Writer")
    var writer: String? = null,

    @SerializedName("Actors")
    @ColumnInfo("Actors")
    var actors: String? = null,

    @SerializedName("Plot")
    @ColumnInfo("Plot")
    var plot: String? = null,

    @SerializedName("Language")
    @ColumnInfo("Language")
    var language: String? = null,

    @SerializedName("Country")
    @ColumnInfo("Country")
    var country: String? = null,

    @SerializedName("Awards")
    @ColumnInfo("Awards")
    var awards: String? = null,

    @SerializedName("Poster")
    @ColumnInfo("Poster")
    var poster: String? = null,

    @SerializedName("Metascore")
    @ColumnInfo("Metascore")
    var metascore: String? = null,

    @SerializedName("imdbRating")
    @ColumnInfo("imdbRating")
    var imdbRating: String? = null,

    @SerializedName("imdbVotes")
    @ColumnInfo("imdbVotes")
    var imdbVotes: String? = null,

    @SerializedName("Type")
    @ColumnInfo("Type")
    var type: String? = null,

    @SerializedName("DVD")
    @ColumnInfo("DVD")
    var dvd: String? = null,

    @SerializedName("BoxOffice")
    @ColumnInfo("BoxOffice")
    var boxOffice: String? = null,

    @SerializedName("Production")
    @ColumnInfo("Production")
    var production: String? = null,

    @SerializedName("Website")
    @ColumnInfo("Website")
    var website: String? = null,

    @SerializedName("Response")
    var response: String? = null,

    @SerializedName("Error")
    var error: String? = null


)
