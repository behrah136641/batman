package com.kapta.batman.data.entity

import com.google.gson.annotations.SerializedName


data class MovieListData(

    @SerializedName("Search") var search: List<SearchData>? = null,
    @SerializedName("totalResults") var totalResults: String? = null,
    @SerializedName("Response") var response: String? = null,
    @SerializedName("Error") var error: String? = null

)
