package com.kapta.batman.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity(tableName = "search",primaryKeys = ["imdbID"])
data class SearchData(

    @SerializedName("imdbID")
    @ColumnInfo("imdbID")
    var imdbID: String ="0",

    @SerializedName("Title")
    @ColumnInfo(name = "Title")
    var title: String? = null,

    @SerializedName("Year")
    @ColumnInfo("Year")
    var year: String? = null,

    @SerializedName("Type")
    @ColumnInfo("Type")
    var type: String? = null,

    @SerializedName("Poster")
    @ColumnInfo("Poster")
    var poster: String? = null

)