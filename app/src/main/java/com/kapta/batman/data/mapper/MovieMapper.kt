package com.kapta.batman.data.mapper

import com.kapta.batman.data.entity.MovieData
import com.kapta.batman.data.entity.MovieListData
import com.kapta.batman.data.entity.SearchData
import com.kapta.batman.domain.entity.Movie
import com.kapta.batman.domain.entity.MovieList
import com.kapta.batman.domain.entity.Search

fun MovieListData.toMovieList() = MovieList(
    response = response,
    totalResults = totalResults,
    error = error,
    search = search?.map {
        it.toSearch()
    }
)

fun SearchData.toSearch() = Search(
    title = title,
    year = year,
    imdbID = imdbID,
    type = type,
    poster = poster
)

fun MovieData.toMovie() = Movie(
    title = title,
    year = year,
    rated = rated,
    released = released,
    runtime = runtime,
    genre = genre,
    director = director,
    writer = writer,
    actors = actors,
    plot = plot,
    language = language,
    country = country,
    awards = awards,
    poster = poster,
    metascore = metascore,
    imdbRating = imdbRating,
    imdbVotes = imdbVotes,
    imdbID = imdbID,
    type = type,
    dvd = dvd,
    boxOffice = boxOffice,
    production = production,
    website = website,
    response = response,
    error = error,
)




