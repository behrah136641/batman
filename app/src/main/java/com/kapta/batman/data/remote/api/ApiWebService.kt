package com.kapta.batman.data.remote.api

import com.kapta.batman.data.entity.MovieData
import com.kapta.batman.data.entity.MovieListData
import okhttp3.RequestBody
import retrofit2.http.*

interface ApiWebService {


    @GET("/")
    suspend fun getMovieList(
        @Query("apikey") apikey: String,
        @Query("s") s: String,
        @Query("page") page: String,
    ): MovieListData?

    @GET("/")
    suspend fun getMovieDetail(
        @Query("apikey") apikey: String,
        @Query("i") id: String,
    ): MovieData?


}