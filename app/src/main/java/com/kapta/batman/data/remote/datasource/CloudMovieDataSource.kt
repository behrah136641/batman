package com.kapta.batman.data.remote.datasource

import com.kapta.batman.data.entity.MovieData
import com.kapta.batman.data.entity.MovieListData
import com.kapta.batman.data.remote.api.ApiWebService
import com.kapta.batman.domain.utils.Constants
import javax.inject.Inject

class CloudMovieDataSource @Inject
constructor(
    private val apiWebService: ApiWebService
) : MovieDataSource {

    override suspend fun getMovieList(s: String, page: String): MovieListData? {
        return apiWebService.getMovieList(Constants.API_KEY, s, page)
    }

    override suspend fun getMovieDetail(id: String): MovieData? {
        return apiWebService.getMovieDetail(Constants.API_KEY, id)
    }

}