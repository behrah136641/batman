package com.kapta.batman.data.remote.datasource

import com.kapta.batman.data.entity.MovieData
import com.kapta.batman.data.entity.MovieListData

interface MovieDataSource {
    suspend fun getMovieList(s: String, page: String): MovieListData?
    suspend fun getMovieDetail(id: String): MovieData?
}