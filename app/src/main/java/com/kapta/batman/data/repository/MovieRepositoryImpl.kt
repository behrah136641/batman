package com.kapta.batman.data.repository

import android.content.Context
import com.kapta.batman.data.db.datasource.contacts.MovieLDataSource
import com.kapta.batman.data.mapper.toMovie
import com.kapta.batman.data.mapper.toMovieList
import com.kapta.batman.data.remote.datasource.MovieDataSource
import com.kapta.batman.domain.entity.Movie
import com.kapta.batman.domain.entity.MovieList
import com.kapta.batman.domain.repository.MovieRepository
import com.kapta.batman.ui.utils.isInternetAvailable
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject


class MovieRepositoryImpl @Inject constructor(
    private val movieLDataSource: MovieLDataSource,
    private val movieDataSource: MovieDataSource,
    @ApplicationContext private val  appContext: Context
) : MovieRepository {
    override suspend fun getMovieList(s: String, page: String): MovieList? {
        return if (isConnectNetwork()) {
            val res = movieDataSource.getMovieList(s, page)
            res?.search?.let { movieLDataSource.insertAll(it) }
            res
        } else {
            movieLDataSource.getMovieList(s,page)
        }?.toMovieList()
    }

    override suspend fun getMovieDetail(id: String): Movie? {
        return if (isConnectNetwork()) {
            val res = movieDataSource.getMovieDetail(id)
            res?.let { movieLDataSource.insertAMovie(it) }
            res
        } else {
           val res= movieLDataSource.getMovieDetail(id)
            res
        }?.toMovie()
    }

    private fun isConnectNetwork(): Boolean {
        return appContext.isInternetAvailable()
    }

}

