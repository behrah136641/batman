package com.kapta.batman.domain.entity

data class MovieList(
    var search: List<Search>?=null,
    var totalResults: String? = null,
    var response: String? = null,
    var error: String? = null
)

data class Search(
    var title: String? = null,
    var year: String? = null,
    var imdbID: String? = null,
    var type: String? = null,
    var poster: String? = null
)