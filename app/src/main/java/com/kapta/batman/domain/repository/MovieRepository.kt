package com.kapta.batman.domain.repository

import com.kapta.batman.domain.entity.Movie
import com.kapta.batman.domain.entity.MovieList

interface MovieRepository {

   suspend fun getMovieList(s: String,page:String): MovieList?
   suspend fun getMovieDetail(id: String): Movie?
}