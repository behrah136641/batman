package com.kapta.batman.domain.usecases.movie

import com.kapta.batman.domain.entity.Movie
import com.kapta.batman.domain.repository.MovieRepository
import com.kapta.batman.domain.usecases.type.BaseUseCase
import javax.inject.Inject

class GetMovieDetailsUseCase @Inject constructor(
    private val movieRepository: MovieRepository
) : BaseUseCase<GetMovieDetailsUseCase.Param, Movie?>() {
    override suspend fun run(inputs: Param) =
        movieRepository.getMovieDetail(inputs.id)

    class Param private constructor(
        val id: String
    ) {
        companion object {
            fun getMovieDetail(id: String) =
                Param(id)
        }
    }
}