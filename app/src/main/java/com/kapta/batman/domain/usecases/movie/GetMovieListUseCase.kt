package com.kapta.batman.domain.usecases.movie

import com.kapta.batman.domain.entity.MovieList
import com.kapta.batman.domain.repository.MovieRepository
import com.kapta.batman.domain.usecases.type.BaseUseCase
import javax.inject.Inject

class GetMovieListUseCase @Inject constructor(
    private val movieRepository: MovieRepository
) : BaseUseCase<GetMovieListUseCase.Param, MovieList?>() {
    override suspend fun run(inputs: Param) =
        movieRepository.getMovieList(inputs.s, inputs.page)

    class Param private constructor(
        val s: String,
        val page: String

    ) {
        companion object {
            fun getMovieList(s: String, page: String) =
                Param(s, page)
        }
    }
}