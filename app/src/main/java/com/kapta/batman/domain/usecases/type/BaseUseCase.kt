package com.kapta.batman.domain.usecases.type

abstract class BaseUseCase<Inputs, Outputs> {

    protected abstract suspend fun run(inputs: Inputs): Outputs

    suspend operator fun invoke(inputs: Inputs) = run(inputs)

}

