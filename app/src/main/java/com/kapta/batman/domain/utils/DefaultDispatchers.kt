package com.kapta.batman.domain.utils

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class DefaultDispatchers @Inject constructor() : CoroutineDispatchers {

    override val background = Dispatchers.IO
}

interface CoroutineDispatchers {
    val background: CoroutineDispatcher
}