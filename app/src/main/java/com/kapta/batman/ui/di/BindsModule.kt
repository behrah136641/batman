package com.kapta.batman.ui.di

import com.kapta.batman.data.db.datasource.contacts.LocalMovieDataSource
import com.kapta.batman.data.db.datasource.contacts.MovieLDataSource
import com.kapta.batman.data.remote.datasource.CloudMovieDataSource
import com.kapta.batman.data.remote.datasource.MovieDataSource
import com.kapta.batman.data.repository.MovieRepositoryImpl
import com.kapta.batman.domain.repository.MovieRepository
import com.kapta.batman.domain.utils.CoroutineDispatchers
import com.kapta.batman.domain.utils.DefaultDispatchers
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface BindsModule {

    @Binds
    fun bindMovieRepository(movieRepositoryImpl: MovieRepositoryImpl): MovieRepository

    @Binds
    fun provideLocalMovieDataSource(localMovieDataSource: LocalMovieDataSource): MovieLDataSource

    @Binds
    fun provideCloudMovieDataSource(cloudMovieDataSource: CloudMovieDataSource): MovieDataSource

    @Binds
    fun bindDefaultDispatchers(defaultDispatchers: DefaultDispatchers): CoroutineDispatchers

}