package com.kapta.batman.ui.di

import android.content.Context
import androidx.room.Room
import com.kapta.batman.data.db.AppDB
import com.kapta.batman.data.db.MovieDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
class DbModule {

    @Provides
    fun provideDatabase(
        @ApplicationContext appContext: Context
    ): AppDB {
        return Room.databaseBuilder(
            appContext,
            AppDB::class.java,
            "batman.db"
        ).fallbackToDestructiveMigration().build()
    }

    @Provides
    fun provideUserDao(db: AppDB): MovieDao {
        return db.movieDao()
    }

}