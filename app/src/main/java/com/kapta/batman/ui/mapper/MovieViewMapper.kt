package com.kapta.batman.ui.mapper

import com.kapta.batman.domain.entity.Movie
import com.kapta.batman.domain.entity.Search
import com.kapta.batman.ui.model.MovieView
import com.kapta.batman.ui.model.SearchView

fun Search.toSearchView() = SearchView(
    title = title,
    year = year,
    imdbID = imdbID,
    type = type,
    poster = poster
)

fun Movie.toMovieView() = MovieView(
    title = title,
    year = year,
    rated = rated,
    released = released,
    runtime = runtime,
    genre = genre,
    director = director,
    writer = writer,
    actors = actors,
    plot = plot,
    language = language,
    country = country,
    awards = awards,
    poster = poster,
    metascore = metascore,
    imdbRating = imdbRating,
    imdbVotes = imdbVotes,
    imdbID = imdbID,
    type = type,
    dvd = dvd,
    boxOffice = boxOffice,
    production = production,
    website = website
)




