package com.kapta.batman.ui.model

data class SearchView(
    var title: String? = null,
    var year: String? = null,
    var imdbID: String? = null,
    var type: String? = null,
    var poster: String? = null
)