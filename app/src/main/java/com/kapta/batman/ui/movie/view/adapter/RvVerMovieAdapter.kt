package com.kapta.batman.ui.movie.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.kapta.batman.R
import com.kapta.batman.ui.di.GlideApp
import com.kapta.batman.databinding.VerMovieItemBinding
import com.kapta.batman.ui.model.SearchView

class RvVerMovieAdapter(val action: (id:String?) -> Unit) :
    RecyclerView.Adapter<RvVerMovieAdapter.RvVerMovieHolder>() {

    private val asyncListDiffer = AsyncListDiffer(this, object : DiffUtil.ItemCallback<SearchView>() {
        override fun areItemsTheSame(oldItem: SearchView, newItem: SearchView): Boolean {
            return oldItem.imdbID == newItem.imdbID
        }

        override fun areContentsTheSame(oldItem: SearchView, newItem: SearchView): Boolean {
            return oldItem == newItem
        }
    })


    inner class RvVerMovieHolder(private val binding: VerMovieItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(searchView: SearchView?) {

            searchView?.let {

                binding.tvTitle.text = String.format("%s", searchView.title)
                binding.tvType.text = String.format("%s", searchView.type)
                binding.tvYear.text=String.format("%s", searchView.year)


                if (searchView.type!="game")
                GlideApp.with(binding.image.context)
                    .load(searchView.poster)
                    .placeholder(ContextCompat.getDrawable(binding.image.context,R.drawable.batman))
                    .fitCenter()
                    .into(binding.image)
            }

            binding.cvParent.setOnClickListener {
                action.invoke(searchView?.imdbID)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RvVerMovieHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        return RvVerMovieHolder(VerMovieItemBinding.inflate(layoutInflater, parent, false))
    }

    override fun onBindViewHolder(holder: RvVerMovieHolder, position: Int) {
        holder.bind(getItem(position))
    }

    private fun getItem(index: Int) = asyncListDiffer.currentList[index]

    override fun getItemCount(): Int {
        return asyncListDiffer.currentList.size
    }

    fun submitList(searchView: List<SearchView>) {
        val list=asyncListDiffer.currentList+searchView
        asyncListDiffer.submitList(list)
    }


}