package com.kapta.batman.ui.movie.view.component

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.kapta.batman.R
import com.kapta.batman.databinding.ToolbarBinding

class Toolbar(context: Context, attrs: AttributeSet?) : ConstraintLayout(context, attrs) {
    private lateinit var viewBinding: ToolbarBinding
    override fun onFinishInflate() {
        super.onFinishInflate()
        viewBinding = ToolbarBinding.bind(this)
        initView()
    }

    private fun initView() {
        setTheme()
    }


    fun setTitle(title: String) {
        viewBinding.textViewTitle.text = title
    }

    fun setListener(toolbarListener: ToolbarListener?) {
        viewBinding.imageViewBack.setOnClickListener {
            toolbarListener?.onBackButtonClickListener()
        }
    }

    fun setIconImageViewBack(resId: Int) {
        viewBinding.imageViewBack.setImageResource(resId)
    }
    fun setIconImageViewMenu(resId: Int) {
        viewBinding.imageViewMenu.setImageResource(resId)
    }

    fun setColor(resId: Int) {
        viewBinding.textViewTitle.setTextColor(ContextCompat.getColor(context, resId))
        viewBinding.imageViewBack.setColorFilter(ContextCompat.getColor(context, resId))
        viewBinding.imageViewMenu.setColorFilter(ContextCompat.getColor(context, resId))
    }


    private fun setTheme() {
        viewBinding.textViewTitle.setTextColor(ContextCompat.getColor(context, R.color.black))
        viewBinding.imageViewBack.setColorFilter(ContextCompat.getColor(context, R.color.black))
        viewBinding.imageViewMenu.setColorFilter(ContextCompat.getColor(context, R.color.black))
    }

    interface ToolbarListener {
        fun onBackButtonClickListener()
    }

}