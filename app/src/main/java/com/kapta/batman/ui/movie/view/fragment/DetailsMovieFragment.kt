package com.kapta.batman.ui.movie.view.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import com.kapta.batman.R
import com.kapta.batman.ui.di.GlideApp
import com.kapta.batman.databinding.FragmentDetailsMovieBinding
import com.kapta.batman.ui.movie.viewmodel.MovieViewModel
import com.kapta.batman.ui.utils.extensionWhen
import com.kapta.batman.ui.utils.isInternetAvailable
import com.kapta.batman.ui.utils.observe
import dagger.hilt.android.AndroidEntryPoint

private const val ARG_ID = "id"

@AndroidEntryPoint
class DetailsMovieFragment : Fragment() {

    private var _binding: FragmentDetailsMovieBinding? = null
    private val binding get() = _binding!!
    private val viewModel: MovieViewModel by activityViewModels()
    private var id: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            id = it.getString(ARG_ID)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailsMovieBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        id?.let { viewModel.getMovieDetail(it) }

        observe(viewModel.movieLiveData) { res ->
            res?.extensionWhen(
                {
                    binding.progressBarLoading.visibility = View.VISIBLE
                    binding.constraintLayout.visibility=View.GONE
                    binding.ivBg.visibility=View.GONE
                    binding.ivShadow.visibility=View.GONE
                },
                {
                    binding.progressBarLoading.visibility = View.GONE
                    binding.constraintLayout.visibility=View.VISIBLE
                    binding.ivBg.visibility=View.VISIBLE
                    binding.ivShadow.visibility=View.VISIBLE

                    it?.let {
                        binding.tvTitle.text=it.title
                        binding.tvProperty.text=it.year+" " +it.genre+" " +it.type+" "+it.runtime
                        binding.tvPlot.text=it.plot
                        binding.tvRate.text=it.imdbRating
                        if (it.type!="game")
                            GlideApp.with(requireContext())
                                .load(it.poster)
                                .placeholder(ContextCompat.getDrawable(requireContext(),R.drawable.batman))
                                .fitCenter()
                                .into(binding.ivBg)
                    }

                },
                {
                    if (!requireContext().isInternetAvailable()) {
                        Toast.makeText(requireContext(), "Details are not saved", Toast.LENGTH_SHORT).show()
                    }

                    this.parentFragmentManager.popBackStack()
                }
            )

        }

        binding.toolbar.root.setIconImageViewBack(R.drawable.ic_heart)
        binding.toolbar.root.setIconImageViewMenu(R.drawable.ic_arrow_back)
        binding.toolbar.root.setTitle("Details")
        binding.toolbar.imageViewMenu.setOnClickListener {
            this.parentFragmentManager.popBackStack()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(id: String) =
            DetailsMovieFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_ID, id)
                }
            }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}