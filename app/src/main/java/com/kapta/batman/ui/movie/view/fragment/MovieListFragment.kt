package com.kapta.batman.ui.movie.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.kapta.batman.R
import com.kapta.batman.databinding.FragmentMovieListBinding
import com.kapta.batman.ui.movie.view.adapter.RvHorMovieAdapter
import com.kapta.batman.ui.movie.view.adapter.RvVerMovieAdapter
import com.kapta.batman.ui.movie.viewmodel.MovieViewModel
import com.kapta.batman.ui.utils.*
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MovieListFragment : Fragment() {
    var page = 1
    private var _binding: FragmentMovieListBinding? = null
    private val binding get() = _binding!!
    private val viewModel: MovieViewModel by activityViewModels()
    private val rvVerMesUserAdapter: RvVerMovieAdapter by lazy {
        RvVerMovieAdapter {
            it?.let {
                replaceFragment(DetailsMovieFragment.newInstance(it))
            }
        }
    }
    private val rvHorMesUserAdapter: RvHorMovieAdapter by lazy {
        RvHorMovieAdapter {
            it?.let {
                replaceFragment(DetailsMovieFragment.newInstance(it))
            }

        }
    }

    private fun replaceFragment(fragment: Fragment) {
        this.parentFragmentManager.beginTransaction().apply {
            if (fragment.isAdded) {
                show(fragment)
            } else {
                add(R.id.container, fragment)
            }
            addToBackStack(null)

            this@MovieListFragment.parentFragmentManager.fragments.forEach {
                if (it != fragment && it.isAdded) {
                    hide(it)
                }
            }
        }.commit()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMovieListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.rvVertical.adapter = rvVerMesUserAdapter
        binding.rvVertical.addItemDecoration(
            ItemDecorationAlbumColumns(
                pxFromDp(requireContext(), 8f).toInt(),
                2
            )
        )
        binding.rvHorizontal.adapter = rvHorMesUserAdapter
        binding.rvHorizontal.addItemDecoration(
            SpacesItemDecoration(pxFromDp(requireContext(), 5f).toInt())
        )

        binding.rvVertical.isNestedScrollingEnabled = false

        binding.nestedScrollView.viewTreeObserver.addOnScrollChangedListener {
            val view =
                binding.nestedScrollView.getChildAt(binding.nestedScrollView.childCount - 1) as View
            val diff: Int =
                view.bottom - (binding.nestedScrollView.height + binding.nestedScrollView
                    .scrollY)
            if (diff == 0 && page < viewModel.totalPage) {
                page++
                viewModel.getMovieList("batman", page.toString())
            }
        }


        binding.toolbar.textViewTitle.text = "Home"

        viewModel.getMovieList("batman", page.toString())

        observe(viewModel.movieListLiveData) { res ->
            res?.extensionWhen(
                {
                    if (page > 1) {
                        binding.progressBarLoadingMore.visibility = View.VISIBLE
                    }
                },
                {
                    binding.nestedScrollView.visibility = View.VISIBLE
                    binding.progressBarLoading.visibility = View.GONE
                    binding.progressBarLoadingMore.visibility = View.GONE
                    if (page == 1) {
                        it?.let { it1 -> rvHorMesUserAdapter.submitList(it1) }
                    }
                    it?.let { it1 -> rvVerMesUserAdapter.submitList(it1) }

                },
                {
                    binding.progressBarLoadingMore.visibility = View.GONE
                }
            )

        }

    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    companion object {

        @JvmStatic
        fun newInstance() = MovieListFragment()
    }
}