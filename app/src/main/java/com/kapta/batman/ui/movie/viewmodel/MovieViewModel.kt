package com.kapta.batman.ui.movie.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kapta.batman.domain.usecases.movie.GetMovieDetailsUseCase
import com.kapta.batman.domain.usecases.movie.GetMovieListUseCase
import com.kapta.batman.domain.utils.CoroutineDispatchers
import com.kapta.batman.ui.mapper.toMovieView
import com.kapta.batman.ui.mapper.toSearchView
import com.kapta.batman.ui.model.MovieView
import com.kapta.batman.ui.model.SearchView
import com.kapta.batman.ui.utils.ResponseResult
import com.kapta.batman.ui.utils.launch
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class MovieViewModel @Inject constructor(
    private val getMovieListUseCase: GetMovieListUseCase,
    private val getMovieDetailsUseCase: GetMovieDetailsUseCase,
    private val coroutineDispatchers: CoroutineDispatchers,

    ) : ViewModel() {

    private val _movieListLiveData = MutableLiveData<ResponseResult<List<SearchView>?>>()
    val movieListLiveData: LiveData<ResponseResult<List<SearchView>?>> = _movieListLiveData

    private val _movieLiveData = MutableLiveData<ResponseResult<MovieView?>>()
    val movieLiveData: LiveData<ResponseResult<MovieView?>> = _movieLiveData

    var totalPage=10

    fun getMovieList(s: String, page: String) {

        _movieListLiveData.value = ResponseResult.Pending

        launch(coroutineDispatchers.background) {
            try {
                getMovieListUseCase(
                    GetMovieListUseCase.Param.getMovieList(
                        s, page
                    )
                ).also { res ->
                    withContext(Dispatchers.Main) {
                        if (res?.response == "True") {
                            if (page=="1") {
                                totalPage= res.totalResults?.toInt()?.div(10) ?: 10
                            }
                            _movieListLiveData.value = ResponseResult.Success(
                                data = res.search?.map {
                                    it.toSearchView()
                                }
                            )
                        } else {
                            _movieListLiveData.value = ResponseResult.Failure(
                                msg = res?.error
                            )
                        }
                    }

                }
            } catch (e: Exception) {
                withContext(Dispatchers.Main) {
                    _movieListLiveData.value = ResponseResult.Failure(e.message)
                }
            }
        }

    }

    fun getMovieDetail(id: String) {

        _movieLiveData.value = ResponseResult.Pending

        launch(coroutineDispatchers.background) {
            try {
                getMovieDetailsUseCase(
                    GetMovieDetailsUseCase.Param.getMovieDetail(
                        id
                    )
                ).also { res ->
                    withContext(Dispatchers.Main) {
                        if (res?.response == "True") {
                            _movieLiveData.value = ResponseResult.Success(
                                data = res.toMovieView()
                            )
                        } else {
                            _movieLiveData.value = ResponseResult.Failure(
                                msg = res?.error
                            )
                        }
                    }

                }
            } catch (e: Exception) {
                withContext(Dispatchers.Main) {
                    _movieLiveData.value = ResponseResult.Failure(e.message)
                }
            }
        }

    }


}