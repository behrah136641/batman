package com.kapta.batman.ui.utils


import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlin.math.ceil


fun <T : Any, L : LiveData<T>> Fragment.observe(liveData: L, body: (T?) -> Unit) {

    liveData.observe(viewLifecycleOwner, Observer(body))
}

fun <T> ResponseResult<T>.extensionWhen(
    onPending: () -> Unit,
    onSuccess: (data: T) -> Unit,
    onFailure: (msg: String?) -> Unit
) {
    when (this) {
        is ResponseResult.Success -> {
            onSuccess.invoke(this.data)
        }
        is ResponseResult.Pending -> {
            onPending.invoke()
        }
        is ResponseResult.Failure -> {
            onFailure.invoke(this.msg)
        }
    }
}
 fun pxFromDp(context: Context, dp: Float): Float {
    return if (dp == 0f) {
        0F
    } else ceil((context.resources.displayMetrics.density * dp).toDouble()).toFloat()

}

fun ViewModel.launch(
    coroutineContext: CoroutineDispatcher,
    body: suspend () -> Unit
) {
    viewModelScope.launch(coroutineContext) {
        body()
    }
}

fun Context.isInternetAvailable(): Boolean {
    var result = false
    val connectivityManager =
        this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        val networkCapabilities = connectivityManager.activeNetwork ?: return false
        val actNw =
            connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return false
        result = when {
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            else -> false
        }
    } else {
        connectivityManager.run {
            connectivityManager.activeNetworkInfo?.run {
                result = when (type) {
                    ConnectivityManager.TYPE_WIFI -> true
                    ConnectivityManager.TYPE_MOBILE -> true
                    ConnectivityManager.TYPE_ETHERNET -> true
                    else -> false
                }

            }
        }
    }

    return result
}





