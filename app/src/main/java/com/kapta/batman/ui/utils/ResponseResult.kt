package com.kapta.batman.ui.utils

sealed class ResponseResult<out H> {

    data class Success<out T>(val data: T) : ResponseResult<T>()

    data class Failure(val msg: String?) : ResponseResult<Nothing>()

    object Pending : ResponseResult<Nothing>()


}